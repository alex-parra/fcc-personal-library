/*
 *
 *
 *       FILL IN EACH FUNCTIONAL TEST BELOW COMPLETELY
 *       -----[Keep the tests in the same order!]-----
 *
 */

var chaiHttp = require('chai-http');
var chai = require('chai');
var assert = chai.assert;
var server = require('../server');

chai.use(chaiHttp);

suite('Functional Tests', function() {
  /*
   * ----[EXAMPLE TEST]----
   * Each test should completely test the response of the API end-point including response status code!
   */
  test('#example Test GET /api/books', function(done) {
    chai
      .request(server)
      .get('/api/books')
      .end(function(err, res) {
        assert.equal(res.status, 200);
        assert.isArray(res.body, 'response should be an array');
        assert.property(res.body[0], 'commentcount', 'Books in array should contain commentcount');
        assert.property(res.body[0], 'title', 'Books in array should contain title');
        assert.property(res.body[0], '_id', 'Books in array should contain _id');
        done();
      });
  });
  /*
   * ----[END of EXAMPLE TEST]----
   */

  suite('Routing tests', function() {
    suite('POST /api/books with title => create book object/expect book object', function() {
      test('Test POST /api/books with title', function(done) {
        const testBook = { title: 'Test Book' };
        chai
          .request(server)
          .post('/api/books')
          .send(testBook)
          .end((err, res) => {
            assert.equal(res.status, 200);
            assert.property(res.body, '_id');
            assert.equal(res.body.title, testBook.title);
            assert.property(res.body, 'created_on');
            done();
          });
      });

      test('Test POST /api/books with no title given', function(done) {
        chai
          .request(server)
          .post('/api/books')
          .send({})
          .end((err, res) => {
            assert.equal(res.status, 400);
            assert.equal(res.body, 'Book title required');
            done();
          });
      });
    });

    suite('GET /api/books => array of books', function() {
      test('Test GET /api/books', function(done) {
        chai
          .request(server)
          .get('/api/books')
          .end((err, res) => {
            assert.equal(res.status, 200);
            assert.isArray(res.body);
            done();
          });
      });
    });

    suite('GET /api/books/[id] => book object with [id]', function() {
      test('Test GET /api/books/[id] with id not in db', function(done) {
        chai
          .request(server)
          .get('/api/books/5de2e6d30e864d10730af1f0')
          .end((err, res) => {
            assert.equal(res.status, 404);
            assert.equal(res.body, 'Book not found');
            done();
          });
      });

      test('Test GET /api/books/[id] with valid id in db', function(done) {
        const testBook = { title: 'Test Book' };
        chai
          .request(server)
          .post('/api/books')
          .send(testBook)
          .end((err, res) => {
            chai
              .request(server)
              .get('/api/books/' + res.body._id)
              .end((err, res) => {
                assert.equal(res.status, 200);
                assert.property(res.body, '_id');
                assert.equal(res.body.title, testBook.title);
                assert.property(res.body, 'created_on');
                done();
              });
          });
      });
    });

    suite('POST /api/books/[id] => add comment/expect book object with id', function() {
      test('Test POST /api/books/[id] with comment', function(done) {
        const testBook = { title: 'Test Book' };
        chai
          .request(server)
          .post('/api/books')
          .send(testBook)
          .end((err, res) => {
            chai
              .request(server)
              .post('/api/books/' + res.body._id)
              .send({ comment: 'Hey there!' })
              .end((err, res) => {
                assert.equal(res.status, 200);
                assert.property(res.body, '_id');
                assert.equal(res.body.title, testBook.title);
                assert.property(res.body, 'created_on');
                assert.isArray(res.body.comments);
                done();
              });
          });
      });
    });
  });
});
