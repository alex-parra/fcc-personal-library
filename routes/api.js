'use strict';

const expect = require('chai').expect;
const ObjectId = require('mongodb').ObjectId;
const db = require('../db');

module.exports = function(app) {
  app
    .route('/api/books')
    .get(async (req, res) => {
      try {
        const books = await db.find('books', {});
        if (!books) throw 'DB is empty';
        const booksData = books.map(book => {
          const { _id, title } = book;
          const commentcount = book.comments ? book.comments.length : 0;
          return { _id, title, commentcount };
        });
        return res.json(booksData);
      } catch (error) {
        return res.json([]);
      }
    })

    .post(async (req, res) => {
      const { title } = req.body;
      if (!title) return res.status(400).json('Book title required');

      try {
        const book = await db.createOrUpdate('books', { title }, { title, created_on: new Date() });
        if (!book) throw 'Create book failed';
        return res.json(book);
      } catch (error) {
        return res.status(500).json(error.message || 'Create book failed');
      }
    })

    .delete(async (req, res) => {
      try {
        const deletedCount = await db.deleteAll('books');
        return res.json('complete delete successful');
      } catch (error) {
        return res.status(400).json('Complete delete failed');
      }
    });

  app
    .route('/api/books/:id')
    .get(async (req, res) => {
      const { id } = req.params;

      try {
        const book = await db.first('books', { _id: db.id(id) });
        if (!book) throw 'Book not found';

        const bookData = { ...book };
        return res.json(bookData);
      } catch (error) {
        return res.status(404).json(error.message || 'Book not found');
      }
    })

    .post(async (req, res) => {
      const { id } = req.params;
      const { comment } = req.body;
      try {
        const book = await db.first('books', { _id: db.id(id) });
        if (!book) throw 'Book not found';
        const comments = (book.comments || []).slice().concat({ comment, created_on: new Date() });
        const updatedCount = await db.update('books', { _id: db.id(id) }, { comments });
        const bookUpdated = await db.first('books', { _id: db.id(id) });
        return res.json(bookUpdated);
      } catch (error) {
        return res.status(500).json(error.message || 'Failed to save comment');
      }
    })

    .delete(async (req, res) => {
      const { id } = req.params;
      try {
        const deletedCount = await db.deleteById('books', id);
        return res.json('delete successful');
      } catch (error) {
        return res.status(500).json(error.message || 'Book delete failed');
      }
    });
};
