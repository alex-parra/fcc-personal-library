var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;
const DB_URL = process.env.DB;

let db;

const init = () => {
  return MongoClient.connect(DB_URL)
    .then(client => {
      console.log('Successful database connection');
      db = client.db('node-personal-library');
    })
    .catch(error => {
      console.log('Database error: ' + error);
      throw error;
    });
};

const id = _id => ObjectId(_id);

const find = async (collection, where = {}) => {
  return await db
    .collection(collection)
    .find(where)
    .toArray();
};

const first = async (collection, where = {}) => {
  return await db.collection(collection).findOne(where);
};

const createOrUpdate = async (collection, where, data) => {
  const { ok, value } = await db.collection(collection).findOneAndUpdate(where, { $setOnInsert: data }, { upsert: true, returnOriginal: false });
  return ok === 1 ? value : undefined;
};

const update = async (collection, where, data) => {
  const { result } = await db.collection(collection).updateOne(where, { $set: data });
  return result.nModified;
}

const deleteById = async (collection, id) => {
  const r = await db.collection(collection).deleteOne({ _id: ObjectId(id) });
  return r.deletedCount;
};

const deleteAll = async (collection) => {
  const r = await db.collection(collection).deleteMany({});
  return r.deletedCount;
}

module.exports = {
  init,
  id,
  find,
  first,
  createOrUpdate,
  update,
  deleteById,
  deleteAll,
};
