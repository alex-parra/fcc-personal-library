const helmet = require('helmet');
const ninetyDaysInMilliseconds = 90 * 24 * 60 * 60 * 1000;

module.exports = app => {
  app.use(
    helmet({
      hidePoweredBy: { setTo: 'PHP 4.2.0' },
      frameguard: { action: 'deny' },
      noCache: true,
      contentSecurityPolicy: {
        directives: {
          defaultSrc: ["'self'"],
          imgSrc: ["'self'", '*.gomix.com', 'glitch.com'],
          styleSrc: ["'self'"],
          scriptSrc: ["'self'", '*.jquery.com'],
        },
      },
    })
  );
};
